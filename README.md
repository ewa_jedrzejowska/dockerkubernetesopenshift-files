# Docker, Kubernetes and OpenShift for Administrators

## Directories

* presentation - contains all pdf files for all parts 
* docker - contains files and command for part I 
* kubernetes - contains files and command for part II 
* openshift - contains files and command for part III 
